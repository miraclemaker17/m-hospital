package logger

import (
	"fmt"
	CONFIG "m-hospital/config"
)

// Log - log based on test value
func Log(str ...interface{}) {
	if CONFIG.Log {
		fmt.Println(str...)
	}
}
