package main

import (
	"math/rand"
	"os"
	"strings"
	"time"

	API "m-hospital/api"
	CONFIG "m-hospital/config"
	DATABASE "m-hospital/database"

	_ "m-hospital/docs"
)

// @title Mahardika Backend API
// @version 1.0
// @description This is a api for Mahardika client/listener/counsellor APIs
// @schemes https
// @host hwmpf9h476.execute-api.ap-south-1.amazonaws.com
// @BasePath /prod
// @securityDefinitions.apikey JWTAuth
// @in header
// @name Authorization
func main() {

	rand.Seed(time.Now().UnixNano()) // seed for random generator

	CONFIG.LoadConfig()
	DATABASE.ConnectDatabase()

	if strings.EqualFold(os.Getenv("lambda"), "1") {
		API.StartServer(true)
	} else {
		API.StartServer(false)
	}
}
