package miscellaneous

import (
	"net/http"
	CONSTANT "m-hospital/constant"
	DB "m-hospital/database"
	_ "m-hospital/model"
	UTIL "m-hospital/util"
)

// ListTopic godoc
// @Tags Miscellaneous
// @Summary Get all available topics
// @Router /topic [get]
// @Produce json
// @Success 200
func ListTopic(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var response = make(map[string]interface{})

	// get topics
	topics, status, ok := DB.SelectSQL(CONSTANT.TopicsTable, []string{"*"}, map[string]string{})
	if !ok {
		UTIL.SetReponse(w, status, "", CONSTANT.ShowDialog, response)
		return
	}

	response["topics"] = topics
	UTIL.SetReponse(w, CONSTANT.StatusCodeOk, "", CONSTANT.ShowDialog, response)
}
