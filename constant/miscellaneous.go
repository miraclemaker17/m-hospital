package constant

// miscellaneous constants
const (
	GSTPercent = 0.18  // 18%
	EventPrice = "100" // INR - amount to be paid by counsellor to create event
)
