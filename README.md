# m-hospital
# For checking server
http://localhost:5000/
# For checking documentation API
http://localhost:5500/docs/

# Glosarry
admin = listener
doctor = counsellor
patient = client

# Completion of instruction checklist :
1. Create User Login & Logout. +1 point if using Oauth2 functionality {done}
2. Create CRUD User management. There are for 3 types of user {done}
3. Create an end-point for making Appointment for Doctor and Patient. Doctor or patient
can cancel the appointment. {on progress}
4. Create an endpoint for patient to check past medical report. {not yet}
5. Creating role management for admin, doctor, and patient. Using dynamic role
management is +1 point. {done}
6. Create an API documentation to explain how the Mahardika Hospital system works.
Explain the business flow, database, and tech stack. Using additional generated API
documentation like Open API Swagger is +1 point. {done}
7. Using DDD or microservice is +1 point. {on progress}
8. Creating Unit Test is +2 point. {on progress}
9. Clean code and complexity will be evaluated. {done}